<?php 
include 'header.php'; 
include 'debug.php'; 
?>
<div id="container">

    <div class="card">

        <?php
        include 'data.php';
        $list_film = getAllFilm();
        ?>
        <!--  var_dump($list_film); pour vérifier -->
        <?php 
        foreach($list_film as $f){ 
        ?>

        <div id="film" class="movie">
            <img src="<?php echo $f["imageURL"]?>" alt="keanu">

            <div class="wrapper">
                <h2><?php echo $f["titre"]; ?></h2>
            </div>
            <div class="wrapper">
                <p><?php echo $f["personnage"]; ?></p>
            </div>

            <div class="wrapper radius">
                <p><?php echo $f["date"]; ?></p>
            </div>

        </div>

        <?php } ?>

    </div>

</div>

<a href="add.php">Ajouter un film</a>

<?php include 'footer.php'; ?>