<?php include 'header.php'; ?>


<div class="box">
    <form action="insert.php" method="post">
        <label for="imgURL">Image : </label>
        <input id="imageURL" type="url" name="imageURL" placeholder="URL de l'image.." required>

        <label for="titre">Titre du film : </label>
        <input id="titre" type="text" name="titre" placeholder="Titre du film.." required>

        <label for="personnage">Personnage : </label>
        <input id="personnage" type="text" name="personnage" placeholder="Nom du personnage.." required>

        <label for="date">Date : </label>
        <input id="date" type="text" name="date" placeholder="Date de sortie.." required>

        <input type="submit" name="Envoyez" id="submit">
    </form>
</div>
</body>
<?php include 'footer.php'; ?>

</html>